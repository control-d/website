#! /bin/bash -x

python -m pip install "pelican[markdown]" pelican-i18n-subsites typogrify markdown-include beautifulsoup4 pymdown-extensions
gem install pygments.rb

git clone -b v2.5.0 https://github.com/alexandrevicenzi/Flex.git
pelican-themes -s Flex/
git clone https://github.com/getpelican/pelican-plugins

pelican -v

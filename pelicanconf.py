AUTHOR = 'Derrick'
SITENAME = 'Control-D'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'US/Eastern'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
#LINKS = (('xkcd', 'https://www.xkcd.com'),
#         ('Pandora', 'https://www.pandora.com'),
#         ('Command-line-fu', 'https://www.commandlinefu.com'),)

# Social widget
SOCIAL = (('gitlab', 'https://gitlab.com/control-d'),
          ('github', 'https://github.com/control-d'),
          ('quay', 'https://quay.io/control-d'),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

STATIC_PATHS = ['images','files','static']
#SUMMARY_MAX_LENGTH = None

#THEME = 'bootstrap-next'
#THEME = 'pelican-bootstrap3'
#THEME = 'elegant'
THEME = 'Flex'
JINJA_ENVIRONMENT = {'extensions': ['jinja2.ext.i18n']}
PLUGIN_PATHS = ['pelican-plugins'] 
PLUGINS = ['i18n_subsites','extract_toc']

MARKDOWN = {
    'extension_configs': {
        'markdown.extensions.codehilite': {'css_class': 'highlight'},
        'markdown.extensions.extra': {},
        'markdown.extensions.meta': {},
        'markdown.extensions.toc': {},
        'markdown.extensions.admonition': {},
        'markdown.extensions.tables': {},
        'markdown_include.include': {'throwException': True, 'base_path': 'content/'},
        'pymdownx.superfences': {},
    },
    'output_format': 'html5',
}

TYPOGRIFY = True
FILENAME_METADATA = '(?P<title>.*)'

# bootstrap-next theme specific settings
#GITHUB_USER = 'control-d'
#GITHUB_SKIP_FORK = True
PYGMENTS_STYLE = 'default'

# elegant theme specific settings
LANDING_PAGE_TITLE = 'My little corner of the Internet'
PROJECTS = [{
    'name': 'homeseer-container',
    'url': 'https://gitlab.com/control-d/homeseer-container',
    'description': 'Container image build artifacts and other files for running Homeseer 4 container image with podman'},
    {'name': 'hexchat-hilight-phrase',
    'url': 'https://gitlab.com/control-d/hexchat-hilight-phrase',
    'description': 'XChat/Hexchat plugin for notification and highlighting on phrases instead of single words'},
    {'name': 'hexchat-pushover-away',
    'url': 'https://gitlab.com/control-d/hexchat-hilight-phrase',
    'description': 'XChat/Hexchat plugin for sending Pushover notifications on private messages or nick mentions while status is away'},
    {'name': 'hexchat-bzlook',
    'url': 'https://gitlab.com/control-d/hexchat-bzlook',
    'description': 'XChat/Hexchat plugin for looking up and displaying information about Bugzilla bugs'},]

# Flex theme spcific settings
SITETITLE = 'control-d'
SITESUBTITLE = 'My little corner of the Internet'
MAIN_MENU = True
MENUITEMS = (
    ("About", "/pages/about.html"),
    ("Projects","/pages/projects.html"),
    ("Tags", "/tags.html"),
    ("Archives", "/archives.html"),
)
LINKS_IN_NEW_TAB = 'external'
COPYRIGHT_YEAR = 2023
PYGMENTS_STYLE = 'default'

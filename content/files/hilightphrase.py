__module_name__ = 'hilight-phrase'
__module_description__ = 'XChat notification and hilighting on phrases instead of single words'
__module_version__ = '2.0'

import xchat, os, re

CONFFILE = os.environ['HOME'] + '/.xchat2/hilight-phrase.conf'
list=[]

xchat.prnt('%(name)s, version %(version)s' % {'name': __module_name__,  'version': __module_version__})  

def read_list():
    try:
        conf = open(CONFFILE,'r')
    except:
        xchat.prnt(CONFFILE + " currently doesn't exist, creating")
        return None
    lines = conf.readlines()
    for each in lines:
        list.append(re.sub(r'\n','',each))
    conf.close()


def save_list():
    conf = open(CONFFILE,'w')
    for phrase in list:
        conf.write(phrase + '\n')
    conf.close()


def check_msg(word, word_eol, userdata):
    for phrase in list:
        if phrase in word_eol[1].lower():
            xchat.command("gui color 3")
            xchat.emit_print( "Channel Msg Hilight", word[0], word[1] )
            return xchat.EAT_ALL
    
    return xchat.EAT_NONE


def add_hilight_phrase(word, word_eol, userdata):
    if len(word) == 1:
        return list_hilight_phrase(word, word_eol, userdata)
    phrase = word_eol[1]
    if phrase not in list:
        list.append(phrase)
        xchat.prnt('\x032* "%s" will be hilighted' % phrase)
    else:
        xchat.prnt('\x032* "%s" is already being hilighted' % phrase)
    save_list()
    return xchat.EAT_XCHAT


def list_hilight_phrase(word, word_eol, userdata):
    xchat.prnt('\x032Current hilight-phrase list: %d hilighted.' % len(list))
    for phrase in list:
        xchat.prnt('\x032  -- %s' % phrase)
    xchat.prnt('\x032* End of hilight-phrase list')
    return xchat.EAT_XCHAT


def remove_hilight_phrase(word, word_eol, userdata):
    phrase = word_eol[1]
    if phrase in list:
        list.remove(phrase)
        xchat.prnt('\x032 "%s" has been removed from the hilight list' % phrase)
    else:
        xchat.prnt('\x032 "%s" is not in the hilight list' % phrase)
    save_list()
    return xchat.EAT_XCHAT


read_list()
xchat.hook_command("hilight-add", add_hilight_phrase)
xchat.hook_command("hilight-list", list_hilight_phrase)
xchat.hook_command("hilight-remove", remove_hilight_phrase)
xchat.hook_print("Channel Message", check_msg)

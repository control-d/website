Title: My Projects
Slug: projects
Status: hidden

<dl>
<dt><h3><i class="fa-brands fa-gitlab">&nbsp;</i><a href=https://gitlab.com/control-d/homeseer-container>homeseer-container</a></h3></dt>
<dd>Container image build artifacts and other files for running Homeseer 4 container image with podman</dd>
<hr>
<dt><h3><i class="fa-brands fa-gitlab">&nbsp;</i><a href="https://gitlab.com/control-d/hexchat-hilight-phrase">hexchat-hilight-phrase</a></h3></dt>
<dd>XChat/Hexchat plugin for notification and highlighting on phrases instead of single words</dd>
<hr>
<dt><h3><i class="fa-brands fa-gitlab">&nbsp;</i><a href="https://gitlab.com/control-d/hexchat-hilight-phrase">hexchat-pushover-away</a></h3></dt>
<dd>XChat/Hexchat plugin for sending Pushover notifications on private messages or nick mentions while status is away</dd>
<hr>
<dt><h3><i class="fa-brands fa-gitlab">&nbsp;</i><a href="https://gitlab.com/control-d/hexchat-bzlook">hexchat-bzlook</a></h3></dt>
<dd>XChat/Hexchat plugin for looking up and displaying information about Bugzilla bugs</dd>
</dl>


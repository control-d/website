Date: 2009-08-23 00:01
Category: Technology
Tags: ubuntu, wiki

I finally updated my wiki page, [Ubuntu Jaunty Jackalope (9.04) on a Toshiba Protege M400]({filename}Ubuntu Jaunty Jackalope \(9.04\) on a Toshiba Protege M400.md), with more information on rotating the screen, and how to get the fingerprint scanner working.

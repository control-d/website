Title: Ubuntu Edgy Eft (6.10) and Feisty Fawn (7.04) on a Toshiba Portege M400 tablet
Date: 2007-05-02 04:54:43
Category: Technology
Tags: linux, ubuntu, wiki
Summary: This is a short guide of my experience of getting Ubuntu 6.10 and 7.04 to run correctly on my Toshiba M400.

[TOC]

## Getting the tablet working

Install the `xserver-xorg-input-wacom` package and reboot.


## Fixing Screen Resolution

This is for the m400 with the SXGA screen with a native resolution of 1400×1050.

Install the `915resolution` package using Synaptic or apt-get.

By running `sudo 915resolution -l` you can see the available video modes. We want mode 58, but we’re going to specify the correct resolution.

Edit `/etc/default/915resolution` so that it says

```
MODE=58
XRESO=1400
YRESO=1050
BIT=
```

Now, edit `/etc/X11/xorg.conf` and put the following at the end of the Screen section.

```xorg.conf
SubSection "Display"
    Depth           24
    Modes           "1400x1050" "1024x768" "800x600" "640x480"
EndSubSection
```

Finally, reboot and you should have the correct 1400×1050 resolution now.


## Rotating the screen and tablet input

*Method 1*: Older method that I got from a friend

Thanks to [Jeff Tickle](http://blog.jefftickle.com/) for these scripts

This script is to go from laptop to tablet mode

```bash
#!/bin/sh

xrandr -o right
for dev in `xsetwacom list dev | sed -r {s/ +[a-z]*//}`; do
    xsetwacom set $dev Rotate CW
    xsetwacom set $dev TopX 0
    xsetwacom set $dev TopY 0
    xsetwacom set $dev BottomX 18500
    xsetwacom set $dev BottomY 24500
done
```

This script is to go from tablet mode back to laptop mode

```bash
#! /bin/sh

xrandr -o normal
for dev in `xsetwacom list dev | sed -r {s/ +[a-z]*//}`; do
    xsetwacom set $dev Rotate NONE
    xsetwacom set $dev TopX 0
    xsetwacom set $dev TopY 000
    xsetwacom set $dev BottomX 24500
    xsetwacom set $dev BottomY 18500
done
```


*Method 2*: New method

I obtained the following script and directions from http://www.math.upenn.edu/~vincentb/UbuntuPortege.html
The screen rotation button now works, and just one script is used to go from laptop to tablet and vice-versa. The directional button will now move the cursor left/right/up/down in tablet mode, too.

```bash
#! /bin/sh

orientation=`xrandr --query | grep "Current rotation" | awk '{print $4}'`
if [ "$orientation" = "normal" ]; then
    /usr/bin/X11/xrandr --orientation right
    xsetwacom set stylus rotate CW
    /usr/bin/X11/xmodmap -e "keycode 11 = Left"
    /usr/bin/X11/xmodmap -e "keycode 13 = Up"
    /usr/bin/X11/xmodmap -e "keycode 10 = Right"
    /usr/bin/X11/xmodmap -e "keycode 12 = Down"
    /usr/bin/X11/xmodmap -e "keycode 14 = Enter"
else
    /usr/bin/X11/xrandr --orientation normal
    xsetwacom set stylus rotate
    /usr/bin/X11/xmodmap -e "keycode 11 = 2 at NoSymbol NoSymbol at twosuperior"
    /usr/bin/X11/xmodmap -e "keycode 13 = 4 dollar NoSymbol currency cent onequarter"
    /usr/bin/X11/xmodmap -e "keycode 10 = 1 exclam NoSymbol exclamdown plusminus onesuperior"
    /usr/bin/X11/xmodmap -e "keycode 12 = 3 numbersign sterling NoSymbol sterling threesuperior"
    /usr/bin/X11/xmodmap -e "keycode 14 = 5 percent NoSymbol threeeights currency onehalf"
```


> So I “chmod a+x” this script, and it now rotates the screen, the Wacom pen and reassigns all the joystick keys! So I assigned this script to the “screen rotation” special key as follows. Go to System Tools-> Configuration Editor, then in apps-> metacity , go to global_keybindings. At run_command_1, write 6 (the name of the “screen rotation” key). Then in keybinding_commands, at command_1 write `sh /location_of_your_scrip/screen`. This will do it! 


I put the script in my `$HOME/bin` directory, and followed the directions for making the screen rotation button work. I also have the Canadian multilingual keyboard, so I didn’t have to change any of the key mappings.


## Getting the fingerprint scanner to work

!!! note
    *Update*: if you’re looking for a guide to getting a fingerprint scanner to work in linux, these instructions will probably work for most distributions. If you’re looking for how to get a fingerprint scanner to work in Hardy Heron (8.04) please check out my updated guide.

Using the [ThinkFinger](http://thinkfinger.sourceforge.net/) driver the fingerprint scanner can be used in lieu of typing in your password in most cases. I obtained the latest package I could find (ThinkFinger 0.2.2) from [here](http://ploum.fritalk.com/thinkfinger_0.2.2_i386.deb). Downloading the lastest source (0.3 at the time of this writing) and compiling it multiple times did not work for me, even after following many other step-by-step directions. After installing the package, a directory needs to be created to store the fingerprints. This can be done with

```console
sudo mkdir /etc/pam_thinkfinger
```

Now, test to that the fingerprint scanner is working correctly type

```console
sudo tf-tool –acquire
```

And you will need to scan your finger 3 times. Now to test that it can verify you’re fingerprint use

```console
sudo tf-tool –verify
```
and scan your finger once. If they match then everything is working properly.

In order to use it instead of typing in your password you’ll need to edit the `/etc/pam.d/common-auth` file to look like this:

```
    #
    # /etc/pam.d/common-auth – authentication settings common to all services
    #
    # This file is included from other service-specific PAM config files,
    # and should contain a list of the authentication modules that define
    # the central authentication scheme for use on the system
    # (e.g., /etc/shadow, LDAP, Kerberos, etc.). The default is to use the
    # traditional Unix authentication mechanisms.
    #
    auth sufficient pam_thinkfinger.so
    auth required pam_unix.so nullok_secure try_first_pass 
```

<s>The stuff to add is in red.</s> Now you’ll need to add your user and fingerprint to the thinkfinger with

```console
sudo tf-tool –add-user <username>
```

Log out and the password prompt should now appear as “Password or swipe finger:” and be able to swipe your finger instead of typing in your password to log in.

Some of the known problems from <https://wiki.ubuntu.com/ThinkFinger>:

> * Biggest problem : gksudo will become “invisible”. It means that if you launch any program with gksudo, it will wait forever until your swipe your finger without displaying anything on the screen. For example, if you launch synaptic from the menu, nothing will happen. Swipe your finger and synaptic appears!
> * Gnome-keyring doesn’t use PAM. So you have to type your password. A workaround for gnome-keyring is available, but this doesn’t work if you authenticated via fingerprint. Same holds for pam_ssh. 

---

There is also a http://alug.appstate.edu/index.php?module=wiki&page=LinuxOnToshibaPortegeM400[version of this page] on the Appalachian Linux Users Group’s wiki. 

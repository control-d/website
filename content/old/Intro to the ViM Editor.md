Title: Intro to the ViM Editor
Date: 2007-05-02 05:27
Modified: 2008-06-07 04:13
Category: Technology
Tags: vim, wiki

[TOC]

## What is Vim?

VIM (Vi IMproved) is a powerful text editor available for \*nix and many other systems, including Windows.  It emulates the behavior of the classic vi editor written by Bill Joy but adds many features.


### Ubuntu Linux

Unfortunately, vim does not come installed on Ubuntu Linux.  What seems to be vim is actual a package called vim-tiny, which is missing some key features of vim, like syntax highlighting.  

To install vim you can use the Synaptics package and search for vim, or open a terminal and run

```console
sudo apt-get install vim
```

### Microsoft Windows 95/98/ME/NT/200/XP

For Windows users who would like to use vim, but would prefer a Graphical User Interface, there is a Windows version of vim which includes Gvim. [Download the Windows version here](http://www.vim.org/download.php#pc)

### References, Guides, and Cheatsheets

* [An Introduction to Display Editing with Vi](http://docs.freebsd.org/44doc/usd/12.vi/paper.html)
    * The original document describing the use of vi.  Does not cover any of the advanced features of Vim, but most features described in this document should be implemented by all vi work-a-likes.
* [Vimdoc Project](http://vimdoc.sourceforge.net)
* [Vim Commands Cheat Sheet](http://www.fprintf.net/vimCheatSheet.html)
* [Vim Reference Card](http://tnerual.eriogerg.free.fr/vimqrc.pdf)
* [Vim Tutor](http://www.vi-improved.org/wiki/index.php/VimTutor)
    * If vim is installed on your system you can run `vimtutor` from the command line to do a 30-min interactive tutorial.


### Sample .vimrc

When vim is run by default it will look for a vimrc file that contains configuration info and scripts that it can use while it is running.  The file can also be found [here]({static}/files/vimrc).  

```vim
{!files/vimrc!}
```

#### Unix/Linux

On \*nix systems this file is called `.vimrc` and found in your home directory, `~/.vimrc`.  Depending on how your system is set up you may not see files starting with a "." when you list the contents of a directory.  To list everything in a directory run the command `ls -a`

If you would like to use the .vimrc supplied here you can run the following command 

```console
wget -O ~/.vimrc http://www.control-d.com/files/vimrc
```

This will get the text file from the server and save it as `.vimrc` in your user's home directory.  
NOTE: if you currently have a .vimrc file in your home directory it will be overwritten with this command.  If this is the case then either your system administrator has provided you with a default vimrc or you have an idea of what you're doing.  From here you can just open it in your favorite editor ;-) and edit as you like.  


#### Windows 95/98/ME/2000/XP

On Windows systems, after installing gVim the configuration file is called `_vimrc` and by default installed to the `C:/Program Files/vim` directory.  

If you would like to use the .vimrc supplied here then save the vimrc file from your browser (File->Save As...) into the Vim directory as `vimrc.vim`.  Now edit the `_vimrc` file and replace the line that says 

```vim
source $VIMRUNTIME/vimrc_example.vim
```

with

```vim
source $VIMRUNTIME/vimrc.vim
```

This will tell vim to also use this file for configuration info.  


### Vim and R

R, a statistics language and environment, can be [integrated with vim]({filename}ViM and R.md).


### IRC

irc.freenode.net     #vim

For more info visit http://www.vim.org


---

There is also a [version of this page](http://acm.appstate.edu/index.php?module=wiki&page=ViImproved) available on ASU's Association of Computing Machinery wiki.


Title: My old 1996 Pontiac Firebird
Date: 2007-07-20 22:56:42
Slug: firebird
Status: hidden

My Car

<img border="1.5" src="../images/mycar.jpg" width="650" height="231">

<u>**Factory Options:**</u>

Firebird V6 Coupe

Factory Code | Description
:----------: | -----------
AAA          | Factory Tinted Windows _(The tint in the picture is 6%, but its gone now.  Factory tint is only ~98%)_
AH3          | Adjuster, 4-way Manual, Driver seat 
AK5          | Restraint System, Front seat, Inflatable, Driveront
AR9          | Seat, Front Bucket, European Style, Passenger & Driver Recline
B35          | Covering, Rear, Floor Mats Carpeted Insert
B84          | Ornamentation, Exterior Molding, Body Side
C49          | Defogger, Rear window, Electric 
C60          | HVAC System, Air Conditioner Front Main Controls
DL5          | Decal, Roadside Service Information 
D35          | Mirror O/S, LH Rem Cont, RH Direct Cont, Painted 
FE9          | Certification Emission, Federal 
F41          | Suspension System, Front, Rear, Firm Ride, Handling _(Replaced front with **KYB** AGX shocks)_
GU5          | Axle Rear, 3.23 Ratio 
G80          | Axle Posi-Traction, Limited Slip 
IPB          | Trim, Interior Design 
J65          | Brake System, Power, Front and Rear Disc 
K68          | Generator, 105 Amp  
L36          | Engine, Gas, 6 Cylinder, 3.8L, SFI, V6 "**3800 Stage II**" 
MM5          | Merchandised Trans, Manual 5 Speed Provisions 
M49          | Transmission, 5-Speed Manual, [Borg-Warner](http://www.borg-warner.com/), 77 MM, 3.75 1st, 0.76 5th, O/D Emission 
NF2          | Emission System, Federal, Tier 1 
NG5          | Emission, Certification Fifty State (50) 
N36          | Steering Wheel, 4 Spokes, Sport 
N60          | Wheel, Aluminum, Painted 
QCB          | Tire, All, P235/55R16 Touring _(Replaced with 235/55R16 Firestone Firehawk SS20's)_
R7N          | Sales Item NO. 39 
STE          | Plant Code - St. Therese, PQ, GM of Canada _(All Camaro's and Firebird's were built in Canada)_
UB3          | Cluster, Instrument, Oil, Coolant Temperature, Volts, Trip Odometer 
U73          | Antenna, Fixed, Radio _(Replaced with shorter, flexible antenna)_
VK3          | License Plate, Front Mounting Package _(Removed)_
VM3          | Label, Consumer, Contains BPR IMP Standard 
V73          | Vehicle Statement, USA/Canada 
W53          | Entertainment System - Radio, AM/FM Stereo, Compact Disc, Equalizer, 4-Speaker System - Coaxial _(Replaced with **Pioneer** DEH-P77DH and 3-way 6.5"s)_
**Y87**      | Performance Enhancement - [3800GT Package](http://tech.firebirdv6.com/y87.html) 
1SB          | Option Package 02 
1SZ          | Option Group Savings 
12B          | Trim Combination, Cloth, Graphite (B)(92)  
12I          | Interior Trim, Graphite (92) 
48Q          | Molding Color, Dark Yellowed Green Metallic (91) 
6GM          | Left Front Spring P/N 22077189 (39 N/mm, 223 lbs/in) 
7GM          | Right Front Spring P/N 22077189 (39 N/mm, 223 lbs/in) 
8TB          | Left Rear Spring P/N 22077418 (16.9 N/mm, 95.4 lbs/in)  
9TB          | Right Rear Spring P/N 22077418 (16.9 N/mm, 95.4 lbs/in)



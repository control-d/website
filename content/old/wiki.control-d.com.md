Date: 2008-06-13 00:00
Category: Technology
Tags: vim, iphone, ubuntu, wiki

The following 4 guides have been moved over to my MediaWiki site and updated:

* Intro to the ViM Editor
* Vim and R
* iPhone 1.1.4 and Ubuntu Linux
* Ubuntu Hardy Heron (8.04) on a Toshiba Portege M400

<http://wiki.control-d.com>

I think it makes them much, much easier to read and follow. 

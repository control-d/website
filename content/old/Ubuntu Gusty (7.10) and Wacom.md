Date: 2007-12-30 00:00
Category: Technology
Tags: linux, wacom

I upgraded my Toshiba Protege M400 to Gusty and to my surprise the wacom input stopped working. After searching the internet without much success, I notice that there were two wacom devices, `/dev/wacom` and `/dev/input/wacom`. Both were symlinks to `/dev/ttyS0`, which makes sense as the tablet is basically a serial device attached to the notebook. In my `/etc/X11/xorg.conf` I had the lines

```xorg.conf
  Option        "Device"        "/dev/wacom"
```

under the wacom cursor, stylus, and eraser input devices. After changing the lines to

```xorg.conf
  Option        "Device"        "/dev/input/wacom"
```

and restarting X, the input started working again. Why? I don’t know, as they both point to `/dev/ttyS0`.

Hopefully this is helpful to someone out there. 

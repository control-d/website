Date: 2010-06-18 00:00
Category: Technology
Tags: xchat, hexchat, wiki

I finally found some free time and updated my hilightphrase XChat python plugin.

The Changes:

1. When listing out the highlighted strings, they are displayed in a separate tab instead of the current tab
2. The list of phrases contains are numbered so that you can delete them using the index instead of typing to whole string back out
3. Added a help command for a short explanation of each command
4. There is only one command now, /hilight, that accepts the options like “add”, “help”, “list”, etc.

The script can be downloaded [here]({static}/files/hilightphrase-2.5.py)

```python
{!files/hilightphrase-2.5.py!}
```

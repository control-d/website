Date: 2011-08-07 19:06
Category: Technology
tags: linux, RHEV, wiki

Wow, it’s been over a year since I’ve posted anything here. Been busy with work and life.

Here's a script called rhevx that I created to painlessly extract the logs gathered by the Log Collector Utility shipped with Red Hat Enterprise Virtualization Manager 2.x

Pass it the zip file created by Log Collector and it will:

1. create and extract everything into a directory with the same name as the zip file
2. find and extract all of the sosreports, if there are any

```bash
    #! /bin/bash

	if [ -z $1 ]; then
		  echo "No input file specified"
		  echo "Usage: $0 <log_collector_output_file>"
		  exit 1
	fi

	if [ -n `which unzip 2> /dev/null` ]; then
		EXTRACTOR="`which unzip` -d "
	elif [ -n `which 7za 2> /dev/null` ]; then
		EXTRACTOR="`which 7za` x -o "
	else
		echo "The unzip or 7za utility is required to run this script, but neither could not be found."
		echo "The unzip utility is usually provided by the unzip package."
		echo "The 7za utility is usually provided by the p7zip package."
		exit 1
	fi

	FILE=$1
	if [ -f $FILE ]; then
		NAME=`basename $FILE .zip`
		mkdir ./$NAME
		$EXTRACTOR "$NAME/" $FILE
		if [ "$?" != "0" ]; then
			echo -e "\nThe file $FILE failed to extract correctly.  Please check that the file is a \
					 \nvalid XZ archive and that it is not corrupted or incomplete.\n\n"
			exit 1
		fi

		# Check to see if it contains any host sosreports
		ls -l $NAME/*.tar.bz2 > /dev/null 2>&1
		if [ "$?" = "0" ]; then
			cd $NAME 
			echo -e "\nUnpacking host sosreports...\n"
			for i in `ls *.tar.bz2`; 
			  do
				echo "Extracting" $i
				FILESIZE=$(/usr/bin/stat -c%s $i | awk '{printf("%d",$1/1024/1024)}')
				echo "This sosreport is $FILESIZE MB compressed"
				tar --totals --totals=SIGUSR1 -xf $i &
				pid=$!
				while ps -p $pid &> /dev/null; do
					sleep 1
					kill -USR1 $pid 2> /dev/null
				done
				wait $pid
				if [ "$?" = "0" ]; then
					echo -e "Done - removing archive\n"
					rm -f $i
				else
					echo -e "\nThe file" $i "failed to extract correctly. Please check the file and \
							 \ntry to extract it manually later. \n\nContinuing...\n\n"
				fi
			done
		else
			echo "This Log Collector archive contains no sosreports"
		fi

		# Check to see if dos2unix is installed
		if [ -n `which dos2unix 2> /dev/null` ]; then
			echo "Converting latest vdc logs to UNIX format"
			dos2unix vdc*txt
		else
			echo "dos2unix not found on this system, so we won't try to convert the latest vdc logs to UNIX format"
		fi

	else
		echo "file $FILE could not be found.  Please check the path and try again."
		exit 1
	fi

	echo -e "\nDone - you can find the extracted logs in $NAME/"
```

If you’re curious enough to try to sift through the logs yourself here is some information from the RHEV Administration Guide that explains some of the more important logs:

<http://docs.redhat.com/docs/en-US/Red_Hat_Enterprise_Virtualization_for_Desktops/2.2/html-single/Administration_Guide/index.html#RHEV-Logs>

You can also find help over IRC on [Freenode](http://freenode.net/irc_servers.shtml) in #rhev


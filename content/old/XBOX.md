Title: XBox Mods
Date: 2007-07-13 16:59:24
Category: Technology
Tags: xbox

## v1.1 XBOX with Xecuter2 Pro inside

*_BIOS_*: X2 4977

*_Dashboard_*: EvolutionX 3935

Mods:

* Samsung 120GB 5400rpm HDD (locked)
* [On/Off RF remote](http://www.xbox-scene.com/articles/rf-power.php)
* Case [fan speed switch](http://www.xbox-scene.com/articles/fanspeed.php)
* [USB-to-XBOX adapter](http://www.xbox-scene.com/articles/xbox-usb-xbox-cable.php)
* Wireless keyboard w/ trackball


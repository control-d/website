Date: 2008-03-27 00:00
Category: Technology
Tags: linux, debian, lighttpd

I noticed that everyday I had a defunct logrotate process on one of my servers running Debian and lighttpd. After some searching I found [this bug report](http://lists.alioth.debian.org/pipermail/pkg-lighttpd-maintainers/2007-April/000543.html) that said to make the following changes to my `/etc/logrotate.d/lighttpd` file:

```
postrotate
            if [ -f /var/run/lighttpd.pid ]; then \
              if [ -x /usr/sbin/invoke-rc.d ]; then \
                 invoke-rc.d lighttpd force-reload > /dev/null 2>&1; \
              else \
                 /etc/init.d/lighttpd force-reload > /dev/null 2>&1; \
              fi; \
            fi;
```

<s>Changes are in red.</s> Just add “2>&1″ to the end of each command. 

Title: Ubuntu Firefox/Shiretoko 3.5 and User Agent string
Date: 2009-08-23 00:00
Category: Technology
Tags: linux, ubuntu, firefox

I installed the firefox-3.5 package in Jaunty and found that the facebook chat applet on their page stopped working. It seemed to think I was running an outdated browser. A quick google search came up with a result: <http://rrenomeron.wordpress.com/2009/07/07/ubuntus-firefox-3-5-and-facebook-chat/>

I could’ve installed the addon, but there’s an easy, simpler way:

1. Type `about:config` in the browser url bar
2. Type `useragent` into the filter bar
3. Edit the `general.useragent.extra.firefox` and change `Shiretoko` to `Firefox`, leaving the rest of the value the same 

![useragent setting in firefox 3.5!]({static}/images/2009/08/useragent.png "useragent setting in firefox 3.5")

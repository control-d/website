Date: 2008-05-26 00:00
Category: Technology
Tags: wiki

Due to the fact that most of my content is guides and that wordpress isn’t properly displaying some of the text, I’m moving my how-tos over to MediaWiki at http://wiki.control-d.com. The other content will stay over here on WordPress. I’ll be making posts when I add new content to the wiki. 

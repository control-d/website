Date: 2009-02-28 00:00
Category: Technology
Tags: linux, iphone

It should go without saying that with “iPhone” and “Linux” in the same sentence you’ll need a jailbroken phone for this. Use the following commands to dump your mp3 file to WAV then convert it to m4a:

1. `mplayer -vo null -vc null -ao pcm:fast:file=file.wav file.mp3`
2. `faac -b 128 -c 44100 -w file.wav`

You’ll end up with a file.m4a file. Once you have the OpenSSH package installed and running on your iPhone you can use the scp command to copy the ringtone to the correct location:

`scp file.m4a root@iphone:/Library/Ringtones/file.m4r`

The root password by default is “alpine”. As you can see the file will need to be renamed to end in “.m4r” instead of “.m4a” to be used as a ringtone. You’ll now be able to see it listed with the other ringtones under Settings -> Sounds -> Ringtone. 

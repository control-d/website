Date: 2009-02-27 00:00
Category: Technology
Tags: linux, ubuntu, fedora

This has already been done before, but I wanted to try to do it myself. It turned out a bit more complicated than I thought it would be on Ubuntu 8.10 and Fedora 10 because of the way gconftool-2 now interacts with D-BUS.

First, I created a script that could be used to change in between a set of predefined background images:

```bash
#!/bin/bash

case $1 in
1)
    gconftool-2 -t str --set /desktop/gnome/background/picture_filename "/home/derrick/wallpaper/Solar_3200x1200_SunRiseTime.png";;
2)
    gconftool-2 -t str --set /desktop/gnome/background/picture_filename "/home/derrick/wallpaper/Solar_3200x1200_DayTime.png";;
3)
    gconftool-2 -t str --set /desktop/gnome/background/picture_filename "/home/derrick/wallpaper/Solar_3200x1200_SunSetTime.png";;
4)
    gconftool-2 -t str --set /desktop/gnome/background/picture_filename "/home/derrick/wallpaper/Solar_3200x1200_NightTime.png";;
*)
    echo "Usage: bgchange.sh [1|2|3|4]";;
esac
```

Then, using the [solution](http://ubuntuforums.org/showpost.php?p=5996758&postcount=12) that allows gconftool-2 to communicate using dbus while being run by cron, I created another script:

```bash
#!/bin/bash

# Export the dbus session address on startup so it can be used by cron
touch $HOME/.Xdbus
chmod 600 $HOME/.Xdbus

env | grep DBUS_SESSION_BUS_ADDRESS > $HOME/.Xdbus
echo 'export DBUS_SESSION_BUS_ADDRESS' >> $HOME/.Xdbus

# Export XAUTHORITY value on startup so it can be used by cron
env | grep XAUTHORITY >> $HOME/.Xdbus
echo 'export XAUTHORITY' >> $HOME/.Xdbus
```

As the post says, I made the script run each time I logged in by selecting System -> Preferences -> Sessions and adding `/home/derrick/bin/.make_Xdbus` as a Startup program.

Next, I edited by crontab by running `crontab -e` and adding the following jobs:

```
00 07 * * * . /home/derrick/.Xdbus; /home/derrick/bin/bgchange.sh 1
00 08 * * * . /home/derrick/.Xdbus; /home/derrick/bin/bgchange.sh 2
00 17 * * * . /home/derrick/.Xdbus; /home/derrick/bin/bgchange.sh 3
00 18 * * * . /home/derrick/.Xdbus; /home/derrick/bin/bgchange.sh 4
```

The `/home/derrick/.Xdbus file` is created each time I log in after adding the previous script as an entry in Sessions. You can adjust at what time the script changes the background to what picture. If you’re not familiar with crontab syntax you can take a look at the crontab(5) man page using the command `man 5 cron`. 

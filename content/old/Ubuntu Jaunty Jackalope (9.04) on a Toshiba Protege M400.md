Date: 2009-07-19 00:00
Category: Technology
Tags: linux, ubuntu, wiki
Summary: Sorry it took me so long, but I’ve updated my wiki with information on how to get most everything working on the tablet. The wacom tablet works out of the box now with no xorg.conf, but it took me a little while to figure out where all of the devices were being created and what they were now called.

<!-- #Modified: 2009-08-26 01:33 -->

## Getting the tablet working

The tablet should work out of the box thanks to a HAL fdi policy installed at `/usr/share/hal/fdi/policy/20thridparty/10-wacom.fdi` that handles the device for us.  There's not even a need for a xorg.conf file.  If you don't have this file make sure you have the latest xserver-xorg-input-wacom package installed.  


## Rotating the screen

I created the following script as `/home/derrick/bin/rotate.sh`

    :::bash
    #! /bin/sh
    
    orientation=`xrandr -q | grep "LVDS"| awk '{print $4}' | sed 's/[^A-Za-z]//g'`
    if [ "$orientation" = "normal" ]; then
        /usr/bin/X11/xrandr --orientation right
        xsetwacom set "Wacom Serial Tablet PC Pen Tablet/Digitizer" rotate CW
        onboard &
    else
        /usr/bin/X11/xrandr --orientation normal
        xsetwacom set "Wacom Serial Tablet PC Pen Tablet/Digitizer" rotate
        killall onboard
    fi

The script checks to see if the orientation is set to "normal", and, if so, rotates the screen and tablet input 90 degrees clockwise.  If it's not set to "normal" it rotates it back.  I've also set it to automatically start onboard, an on-screen keyboard when rotated.  It then closes it when rotating it back to normal.  I found the device names by looking at the output of the command `xinput list`.

From here getting the rotate screen button to work is somewhat of a hack.  There seems to be a bug in this version where the button triggers two key events, *Super_L* and *6*, at the same time.  I banged my head over this for a while.  In the past it produced one distinct key event, and I would map this in Gnome to run a command.  Now, the problem is it's not possible in Gnome to map, say Super+6, to the rotate script.  You obviously don't want to map the 6 key for this, so how about just &lt;Super&gt;?  That won't work either as the multi-directional button next to it generates Super+1 through Super+5, depending on what direction you press it.  What I finally ended up doing was mapping Super_L to modifier key Mod4 using xmodmap:  

1. Create a .Xmodmap file in your home directory (ie., `/home/derrick/.Xmodmap`)
2. Edit the .Xmodmap file and put the following:

        clear Mod4
        add Mod4 = Super_L

3. Run the command `xmodmap ~/.Xmodmap`


This then allows us to create a Gnome keyboard shortcut using this new Mod4 modifier key:

1. Go to *System -> Preferences -> Keyboard Shortcuts*
2. Click Add, give it a name, and in the Command field put the full path to your executable rotate.sh script
3. Run the command `gconf-editor`
4. Go to `/desktop/gnome/keybindings/` and find the * custom**N** * folder that contains the custom keyboard shortcut that you created
5. Edit the **binding** value and enter the following value

        <Mod4>6

Now the rotate script will be executed whenever you press the rotate button.


#### Setting wacom tablet pen functions

I like for the eraser to act as the mouse right-click, but by default it does the same left-click as the pen tip.  This functionality can be set using the following command:

    :::console
    xsetwacom set "Wacom Serial Tablet PC Pen Tablet/Digitizer eraser" Button1 "button 3"


This setting goes away each time the system is rebooted, unfortunately.  I've set it be executed each time I log into gnome by adding it to the session preferences.

1. Go to System->Preferences->Startup Applications under the Gnome menu.
2. Click "Add" under Startup Programs
3. paste the following command:

        :::console
        xsetwacom set "Wacom Serial Tablet PC Pen Tablet/Digitizer eraser" Button1 "button 3"

Now when clicking the eraser on the screen the right-click menu is displayed.  


#### Using the fingerprint reader

There's a lot of bad/outdated information on the web about this. I've read through all that information and distilled it down into this working solution for 9.04.  


1. Do not use the thinkfinger packages that are in the jaunty repository.  Instead, add the following repos to your `/etc/apt/sources.list` file:

        deb http://ppa.launchpad.net/jon-oberheide/ppa/ubuntu jaunty main
        deb-src http://ppa.launchpad.net/jon-oberheide/ppa/ubuntu jaunty main

2. Add the key for this repository using the following command:

        :::console
        sudo apt-key adv --recv-keys --keyserver keyserver.ubuntu.com F2A12FABE936A7BBDC4EA33351DB180D1323E149

3. Update the apt cache data and install the required packages:

        :::console
        sudo apt-get update && sudo apt-get install thinkfinger-tools libpam-thinkfinger

4. Create the file `/etc/udev/rules.d/60-thinkfinger.rules` with the following contents:

        #
        # udev rules file for the thinkfinger fingerprint scanner
        #
        
        # uinput device
        KERNEL=="uinput", MODE="0660", GROUP="plugdev"

5. Reboot the system

6. As your user create the fingerprint file

        :::console
        $ tf-tool --acquire
                
        ThinkFinger 0.3 (http://thinkfinger.sourceforge.net/)
        Copyright (C) 2006, 2007 Timo Hoenig <thoenig@suse.de>

        Initializing... done.
        Please swipe your finger (successful swipes 3/3, failed swipes: 0)... done.
        Storing data (/home/derrick/.thinkfinger.bir)... done.

7. Test that it works

        :::console
        $ tf-tool --verify

        ThinkFinger 0.3 (http://thinkfinger.sourceforge.net/)
        Copyright (C) 2006, 2007 Timo Hoenig <thoenig@suse.de>

        Initializing... done.
        Please swipe your finger (successful swipes 1/1, failed swipes: 0)... done.
        Result: Fingerprint does match.

8. Edit the `/etc/pam.d/common-auth` file and add the line "auth	sufficient	pam_thinkfinger.so"  where indicated below:

        ....
        # here are the per-package modules (the "Primary" block)
        auth	sufficient	pam_thinkfinger.so
        auth	[success=1 default=ignore]	pam_unix.so try_first_pass nullok_secure
        # here's the fallback if no module succeeds


You will now be able to login to gnome and unlock gnome-screensaver with your fingerprint.

**Caveats:**

* if the first swipe fails it falls back to password auth only
* after logging into gnome by swiping your finger you will be prompted to enter your password for the gnome keyring

After some testing I might be adding how to change the power level so that the reader no longer gets hot.  



#### Turn off IPv6

You can disable the unnecessary IPv6 traffic by adding the following line to the end of the `/etc/modprobe.d/blacklist.conf` file:

    blacklist ipv6


You should now at least notice that web pages load faster, as the system is no longer trying to do a IPv4 *and* IPv6 DNS lookups.


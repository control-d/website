Date: 2007-11-24 03:18:13
Category: Technology
Tags: iphone, linux, wiki

!!! warning ""
    The following guide is [deprecated](http://en.wikipedia.org/wiki/Deprecated), and the instructions here should not be used on a 1.1.3 or 1.1.4 iPhone. There are much easier steps in windows now, and running ‘passwd’ on a 1.1.3/1.1.4 iphone will cause the menu (Springboard) to continuously restart. If you have a 1.1.2 iPhone that is not jailbroken yet, then just update to 1.1.4 with iTunes.

I recently purchased an iPhone hearing that it was possible to use it in linux. Unfortunately, that goal turned out to be a lot harder to achieve than I had thought. I waded through quite a few personal accounts and guides that were either incomplete, outdated, or just plain wrong. I don’t take credit for any of the instructions in this guide. All I’m doing is laying out the steps I followed to jailbreak my iPhone, install the software I needed, and get it working with AT&T again. As with any set of instructions, please read all the way through them before attempting this. This was done with iTunes 7.5, Ubuntu Fiesty (7.04), and gtkpod 0.99.8.

### Software required:

* Windows XP

    I was not able to get iTunes 7.3 or 7.5 working in the latest version on Wine. There’s also a bug in VMware where iPhones don’t show up correctly. That leaves us with having to use a bare metal install of Windows XP, unfortunately.

* [Java](http://www.java.com/en/download/index.jsp)

    Jailbreak requires Java in Windows

* [iTunes](http://www.apple.com/itunes/download/)

    This guide is setting up an iPhone to use with AT&T. The quick and dirty way is to *shudder* install iTunes.

* [1.1.2 Jailbreak](http://conceitedsoftware.com/iphone/1.1.2-jailbreak.zip)
* [Unpatched iPhone 1.1.1 firmware]({static}/files/iPhone1,1_1.1.1_3A109a_Restore.ipsw)

    This is needed to “restore” your new iPhone with the 1.1.2 firmware down to 1.1.1. Don’t worry, we’ll update it again.

* [IBrickr](http://ibrickr.com/downloads/ibrickr_v0.91.zip)

    This will only be used to reset the iPhone. None of its other features work with the 1.1.1 or greater firmware currently. 


#### Steps in Windows:

1. Install Windows XP, duh.
2. Install iTunes.
3. Hook up the iPhone and activate it with iTunes. Leave ITunes running.
4. Download the 1.1.1 firmware with the link I provided above.
5. Bring the iPhone into Recovery Mode by holding down the Power button and Home button. Release the Power button when the screen goes blank, and continue holding down the Home button until iTunes sees that the iPhone is in recovery mode.
6. Hold the shift key and click on the Restore button. Then select the 1.1.1 firmware restore file you downloaded. iTunes will load the 1.1.1 firmware onto your phone, and error out at the end. This is expected. You won’t be able to do anything with the phone until we reset it.
7. Download and unzip iBrickr if you haven’t already. Then goto Start->Run, and type the command “cmd”. cd to the directory you unzipped iBrickr into and run the ‘jailbreak.exe’ program. This should connect to the iPhone and bring up a prompt. At the prompt enter these three commands:
    * `cmd setenv auto-boot true`
    * `cmd saveenv`
    * `cmd fsboot`
    
    The iPhone will reboot, and everything should work except for the phone. Exit the jailbreak program.

8. Connect to a wireless network with internet access. Go to http://jailbreakme.com, scroll down to the bottom, click “Install AppSnapp”. Safari will exit and you’ll return to your springboard.
9. Run the new Installer app that was just installed. Go to “Tweaks (1.1.1)” and install OktoPrep.
10. Connect the phone to iTunes. It will tell you there’s an update available (1.1.2). Click on Update and wait for that to finish. Activate the phone again.
11. Download and unzip Jailbreak if you haven’t already. Run the windows.bat that was unzipped, and follow the instructions.
12. Fin. 

Note: If you want to purchase stuff from iTunes directly from your iPhone go ahead and set up an account through iTunes on the computer and connect and disconnect the iPhone.

Now you can blow away this windows install and boot into linux.

#### Steps in Linux:

1. Install the fuse, sshfs, and gtkpod

    `sudo apt-get install fuse sshfs gtkpod`

2. Add yourself to the fuse group

    `sudo adduser <username> fuse`

3. log out and back, or `su – <username>` so that you’re authenticated as being in the fuse group. It’s just easier to log out and back in.
4. Connect the iPhone to your wireless network if it’s not already connected. Also, change the settings to never Auto-lock the screen. When it auto-locks it disconnects from the network.
5. Run the command `host iphone`. If this returns the IP address of your iPhone then great. If not, then you’ll need to look it up manually. Tapping the blue arrow next to the SSID of the connected network will give you this.
6. SSH into the phone.

    `ssh root@iphone or ssh root@<ip address>`
    
    There should be an SSH app on your iPhone that will allow you to change the password. If not, the default password is set to “dottie”. As usual, you can change it by running ‘passwd’ once you’ve logged in.

7. Move to where the iTunes library is

    `cd /var/root/Media`

8. Make a symbolic link to this directory from within itself

    `ln -s . iTunes`

9. Make another symbolic link to the iTunes_Control directory called iPod_Control

    `ln -s iTunes_Control iPod_Control`

10. Quit

    `exit`

11. Mount the filesystem over wireless with sshfs

    `sshfs root@iphone:Media <mountpoint>`
    
    You could mount on a new directory in your home directory, or you could mount it somewhere like /media/iphone. Just make sure if it’s not in your home directory that you manually chown it before mounting it for the first time.
    
    `sudo chown <username> /path/to/mountpoint`

12. Start gtkpod, and load an ipod from that mounted directory. Now you can add and remove music. You’ll get errors about artwork as it doesn’t yet support it on the iPhone yet.
13. Quit gtkpod, and unmount the iphone filesystem

    `sudo umount <mountpoint>`
    
    or
    
    `fusermount -u <mountpoint>`

14. Enjoy 

Everything else you could want to do can be done with the apps you can install straight on the iPhone with the Installer app, including setting custom ringtones from the music you put on there with gtkpod.

This guide is kind of crude at this point, and I’ll try to clean it up some in the future. Feel free to leave questions or comments on how to make these directions better, and I’ll do my best to reply. 

Date: 2007-05-02 05:44:01
Category: Technology
Tags: vim, wiki

[TOC]

## Ubuntu Linux

This guide has been updated for Hardy Heron (8.04)


### Installing R and vim-r-plugin

Install R with the following command:

    :::console
    sudo apt-get install r-base


Then download the newest vim-r-plugin package from <http://www.uft.uni-bremen.de/chemie/ranke/debs/sid-jr/vim-r-plugin_0.5-79_all.deb> with the following command:

    :::console
    wget http://www.uft.uni-bremen.de/chemie/ranke/debs/sid-jr/vim-r-plugin_0.5-79_all.deb


We'll now use the gdebi GUI to install the package because it will install the dependencies for us

    :::console
    gdebi-gtk vim-r-plugin_0.5-79_all.deb


Then use the new vim-addon-manager utility to add the plugin to vim

    :::console
    sudo vim-addons -w install r-plugin



### Using Vim and R

First, open vim as you normally would.  Then, run the vim command `set ft=r`.  This will tell vim you're working on a R file.  If you're using the .vimrc file provided on the [[Vim and R]] page then this will be done automatically for you whenever you open a \*.r file.  Now, press the **F2** key.  This will open an instance of R in a separate window that you can now interact with.  After typing a series of R commands in vim you can run all of them in R by pressing the **F5** key, or you can just run a few lines of the file by highlighting the lines you want in Visual mode and pressing the **r** key.  


## Issues
* If you're getting the following error, `Error in X11() : could not find any X11 fonts**`, then you need to change the font paths in the "Files" section of your `xorg.conf` file from `.../X11/fonts/...` to `.../fonts/X11/...`
* The vim-r plugin brings R up in an xterm console.  To copy/paste, highlight the text you want from R, then click the middle mouse button to paste.  


---

A [version of this page]([http://acm.appstate.edu/index.php?module=wiki&page=VimAndR) is available on Appalachian State University's Association of Computing Machinery wiki.


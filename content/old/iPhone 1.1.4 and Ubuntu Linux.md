Date: 2008-05-28 00:21
Modified: 2009-06-01 19:29
Category: Technology
Tags: linux, iphone, wiki

!!! warning ""
    This guide is horribly out of date, and much more concise and updated instructions can be found at <https://help.ubuntu.com/community/PortableDevices/iPhone>

I'm writing this guide after upgrading my 1.1.2 jailbroken iPhone to 1.1.4.  As with [my last guide]({filename}iPhone 1.1.2 and Ubuntu Linux.md), I don't claim to have discovered any of these steps.  I'm just taking various random, steps from other guides and putting them into what I think is a concise, straight-forward approach.  This upgrade will require you to reset your iPhone to the factory firmware (1.1.4 in this case), so you will want to sync it with iTunes right before following these steps so that all your settings can be restored.  When I synced mine, all music that I had loaded using the method from [my last guide]({filename}iPhone 1.1.2 and Ubuntu Linux.md) was not backed-up, and my iTunes database was reset.  I'm sure for those following this guide all your music is sitting safely on your linux partition or media server, so it shouldn't be an issue.  If you purchased music on the phone, it will be synced if you tell iTunes to sync your "Purchased" playlist.  *I used iTunes 7.5, Ubuntu 7.10, ZiPhone 2.6b, and gtkpod 0.99.12.  This guide may not be applicable to Ubuntu Hardy Heron (8.04).*


<u>Software required</u>:

1. Windows XP

    I used bare-metal, but the VMWare [bug report](http://communities.vmware.com/thread/91715) I referenced last time suggested that syncing an iPhone with a guest is working now, and reflashing the firmware may also be possible.  

2. [iTunes](http://www.apple.com/itunes/download/)

    Again, I used iTunes 7.5 since it was already installed and initially synced with my phone.  I haven't read anything that would suggest a new version, such as 7.6, would matter.

3. [ZiPhone](http://download.ziphone.org/)
    This is the only program you need to unlock and jailbreak your iPhone


<u>Steps in Windows</u>:

1. Install Windows XP
2. Install iTunes
3. Plug up the iPhone
    * If this is new 1.1.4 iPhone, activate it with iTunes.  Leave ITunes running.
    * If this is an upgrade from 1.1.1 - 1.1.3 to 1.1.4, sync it with iTunes to back up all of your iPhone-specific settings.  Then click "Restore" to reset the phone to factory settings with the latest firmware update, 1.1.4.  The firmware will need to download and then be loaded onto the phone.  This process can take a while.  After the restore is finished, you'll be prompted to restore from your backup if you synced it.  This will leave you with what you had before, minus custom applications (and maybe music, as I noted above.)  Leave iTunes running.  
4. Run the ZiPhone executable you downloaded earlier

    It's a self-extracting zip.  If it doesn't already start, run the `ZiPhoneGUI.exe` in the extracted directory. 

5. Select the appropriate action for what you want

    I chose "Jailbreak" because I use AT&T and just wanted the 1.1.4 firmware and Installer.app.  As the directions state, it should not take more than a couple of minutes. 
 
6. Fin.


Now some things will need to be set up on the iPhone before we can sync music.  

<u>Steps on iPhone:</u>:

1. Connect to a wireless network with Internet access.  Also, change the settings to never Auto-lock the screen.  When it auto-locks it disconnects from the network.  
2. Start Installer.app, wait for sources to refresh.  If prompted to upgrade Installer.app, do so.  Wait for that to finish, and leave and restart Installer.app
3. Go to Sources, tap Edit, remove the Conceited Software repo.  

    The repo moved, and the next step will update it.

4. Go to the Sources listing, install "Community Sources"
5. Go to the System listing, install "BSD Subsystem"
6. Go to the System listing, install "OpenSSH"
7. Fin.

WARNING: DO NOT run the command `passwd` to change the password on your iPhone.  The utility is broken and will cause the menu, Springboard, to enter a cycle where it continuously restarts.  If this happens you will have to restore your iPhone, and jailbreak it again.  


<u>Steps in Linux</u>:

1. Run the following command

    `host iphone`

    If this returns the IP address of your iPhone then great.  If not, then you'll need to look it up manually.  Tapping the blue arrow next to the SSID of the connected network will give you this.  

2. Remove the libgpod package, if it exists

    `sudo apt-get remove libgpod`

3. Add the apt repo that contains the ipod-convenience and gtkpod packages that we'll need
4. Edit `/etc/apt/sources.list`, and add the following line

        deb http://ppa.launchpad.net/ipod-touch/ubuntu gutsy main

    then run 

        sudo apt-get update

    to update the package list.  

5. Install the fuse, sshfs, gtkpod, and ipod-convenience packages

    `sudo apt-get install fuse sshfs gtkpod ipod-convenience`

    The ipod-convenience package configuration will prompt you for the IP address or hostname of your iPhone, along with a mount directory.  The IP address is going to be what you found earlier, or you can just put "iphone" if DNS resolved it correctly in the earlier step.  I put the mount point in my home directory, as I knew gtkpod would find it automatically.  If you need to change these setting later they can be found in `/etc/default/ipod-convenience`

6. Add yourself to the fuse group

    `sudo adduser <username> fuse`

7. log out and back, or run `su - <username>` so that you're authenticated as being in the fuse group.  It's just easier to log out and back in.  
8. SSH into the phone.  

    `ssh root@iphone` or `ssh root@<ip address>`

    The initial connect could take 30-60 seconds while it generates the keys.  The default password is set to "alpine".  DO NOT try to change this with `passwd`!  

9. Close the connection by typing `exit`
10. With the iPhone connected to your wireless network, plug it in via usb on your computer.
11. Mount the iphone with the following command:

    !!! warning
        At the time of this writing `/usr/bin/iphone-mount` has a bug in it.
        
        Change /usr/bin/iphone-mount line 66 from:
        
            if ssh root@192.168.#.# test -d /var/mobile; then
        
        to:
        
            if ssh root@$IPADDRESS test -d /var/mobile; then

    `iphone-mount`

12. Enter the root password, "alpine," when prompted.  The first time running this will set up the directory structure and iTunes db key.  
13. Umount the iPhone with the following command:

    `iphone-unmount`

14. Done


From here you can disconnect the usb, mount it with `iphone-mount`, enter the root password when prompted, run gktpod and sync up your music.  The linux steps and more can be found at <https://help.ubuntu.com/community/PortableDevices/iPhone>.  The include directions for using Amarok and how to set up ssh on your iPhone for passwordless access.


Date: 2009-05-26 00:00
Category: Technology
Tags: xchat, hexchat

I wrote the following xchat plugin after they decided to [“fix” the Extra words to highlight feature](http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=487950) in XChat 2.8.6

The python plugin can be downloaded [here]({static}/files/hilightphrase.py)

```python
{!files/hilightphrase.py!}
```

You can use the plugin by either:

* manually loading the file through the menu (Windows -> Plugins and Scripts) every time you restart xchat
* copy the hilightphrase.py to your `$HOME/.xchat2/` direcory where it will get loaded automatically when xchat is run.

Once loaded, a `/hilight-list` will list all of the currently active phrases to highlight. A `/hilight-add <phrase>` where _<phrase>_ is a string will add the specified phrase to the list. As you can probably guess, a `/hilight-remove <phrase>` where _<phrase>_ is a currently active phrase will remove it from the list.

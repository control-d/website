Title: Website migration
Date: 2022-09-21
Category: Technology

I've migrated my website from a very old, very broken Wordpress deployment to [Pelican](http://getpelican.com/), a static site generator written in Python. All of my wiki content from wiki.control-d.com (previously hosted on an even more broken Wikimedia deployment) has been converted over, as well. No promises on new content, though. 
